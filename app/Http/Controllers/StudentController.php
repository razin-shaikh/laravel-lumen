<?php

namespace App\Http\Controllers;

use App\Models\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $students = Student::all();
        return response()->json($students, 201);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dump($request->all());
        $this->validate($request, [
            'name' => 'required|min:3',
            'email' => 'required|email|unique:students',
            'age' => 'required|numeric|min:5|max:30',
            'phone' => 'required|digits:10'
        ]);

        $student = new Student();
        $student->name = $request->name;
        $student->email = $request->email;
        $student->age = $request->age;
        $student->phone = $request->phone;
        $student->save();

        return response()->json($student, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function show(Student $student, $id)
    {
        $id = ['id' => $id];
        $validator = Validator::make($id, [
            'id' => 'numeric'
        ]);
        if ($validator->fails()) {
            return $validator->errors();
        }

        $student = Student::find($id);
        return response()->json($student, 201);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function edit(Student $student)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Student $student, $id)
    {
        $request['id'] = $id;
        $this->validate($request, [
            'id' => 'required|numeric',
            'name' => 'required|min:3',
            'email' => 'required|email|unique:students,email,' . $request->id,
            'age' => 'required|numeric|min:5|max:30',
            'phone' => 'required|digits:10',
        ]);

        $student = Student::find($request->id);
        if(!$student){
            return response()->json('Student Not Found',201);
        }
        $student->name = $request->name;
        $student->email = $request->email;
        $student->age = $request->age;
        $student->phone = $request->phone;
        $student->update();

        return response()->json($student, 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student $student,$id)
    {
        $id = ['id' => $id];
        $validator = Validator::make($id, [
            'id' => 'numeric'
        ]);
        if ($validator->fails()) {
            return $validator->errors();
        }

        $student = Student::find($id);
        if(!$student){
            return response()->json('Student Not Found',201);
        }
        
        $rsp = Student::destroy($id);
        if(!$rsp){
            return response()->json('Somthing went wrong while deleting student',201);
        }

        return response()->json('Deleted Successfully',201);

    }
}
