<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use App\Http\Controllers\StudentController;
use Illuminate\Support\Facades\Route;

$router->get('/', function () use ($router) {
    // return $router->app->version();
    // return  $routes;
    $routeCollection = Route::getRoutes();
    foreach ($routeCollection as $value) {
        // echo $value->getPath();
        dump($value);
        
    }
});
$router->group(['prefix' => 'api'], function () use ($router) {
    // get all students
    $router->get('students',  ['uses' => 'StudentController@index']);

    //get single student
    $router->get('students/{id}', ['uses' => 'StudentController@show']);

    //create new student
    $router->post('students', ['uses' => 'StudentController@store']);

    //update student details
    $router->put('students/{id}', ['uses' => 'StudentController@update']);

    // delete single student
    $router->delete('students/{id}', ['uses' => 'StudentController@destroy']);
});
